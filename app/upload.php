<?php
 
 
 if (isset($_POST['submit']) && isset($_FILES['foto'])) {
	include "conexaodb.php";

	echo "<pre>";
	print_r($_FILES['foto']);
	echo "</pre>";

	$img_name = $_FILES['foto']['name'];
	$img_size = $_FILES['foto']['size'];
	$tmp_name = $_FILES['foto']['tmp_name'];
	$error = $_FILES['foto']['error'];
//testei as mensagens de erro e funcionaram normalmente.
	if ($error === 0) {
		if ($img_size > 125000) {
			$em = "Desculpe, seu arquivo é muito grande.";
		    header("Location: index.php?error=$em");
		}else {
			$img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
			$img_ex_lc = strtolower($img_ex);

			$allowed_exs = array("jpg", "jpeg", "png"); 

			if (in_array($img_ex_lc, $allowed_exs)) {
				$new_img_name = uniqid("IMG-", true).'.'.$img_ex_lc;
				$img_upload_path = 'uploads/'.$new_img_name;
				move_uploaded_file($tmp_name, $img_upload_path);

				// insere no banco de dados
				$sql = "INSERT INTO imagens(img_dir) 
				        VALUES('$new_img_name')";
				mysqli_query($conn, $sql);
				header("Location: ../galeria.php");
			}else {
				$em = "Você não pode carregar arquivos desse tipo.";
		        header("Location: index.php?error=$em");
			}
		}
	}else {
		$em = "Ocorreu um erro desconhecido!";
		header("Location: index.php?error=$em");
	}

};//else {
	//header("Location: index.php");
//}

?>