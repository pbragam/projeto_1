<?php  include "app/conexaodb.php"; ?>
<!DOCTYPE html>
<html  lang="pt-BR">
    <head>
		<title>Galeria de fotos</title>
		<meta name="author" content="Equipe de projeto">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="styles/estilos.css">
		<script src="js/codigo.js"></script>
		<!--abaixo é o css de como serão vistas as imagens carregadas -->
		<style>
		body {
			display: flex;
			justify-content: center;
			align-items: center;
			flex-wrap: wrap;
			min-height: 100vh;
		}
		.alb {
			width: 200px;
			height: 200px;
			padding: 5px;
		}
		.alb img {
			width: 100%;
			height: 100%;
		}
		a {
			text-decoration: none;
			color: black;
		}
	</style>
	</head>
	<body>
		<div id="content">
		<?php include("app/pagina.menu.php"); ?>

		<a href="index.php">&#8592;</a>
     <?php 
          $sql = "SELECT * FROM imagens ORDER BY id DESC";
          $res = mysqli_query($conn,  $sql);

          if (mysqli_num_rows($res) > 0) {
          	while ($images = mysqli_fetch_assoc($res)) {  ?>
             
             <div class="alb">
             	<img src="uploads/<?=$imagens['img_dir']?>">
             </div>
          		
    <?php } }?>
    		<!--TODO: apesar das imagens entrarem irem pro banco de dados, não está indo mais pra pasta uploads, e a imagem não carrega, algo que já vinha acontecendo desde que criei o infouser para mostrar o preview da imagem, tentei resolver só mas não consegui-->
			<div class="error"><?php if(isset($_GET['error'])) echo $_GET['error']; ?></div>
		</div>
		<footer>
		</footer>
	</body>
</html>